# Repositorio plantilla: "Búsqueda de palabras"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/algoritmos-ii/buscapalabras/README.md), que incluye la fecha de entrega.
